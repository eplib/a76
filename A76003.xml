<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A76003">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>A letter from General Monck from Dalkeith, 13 October 1659. Directed as followeth. For the Right Honorable William Lenthal, Esquire, Speaker; to be communicated to the Parliament of the Common-wealth of England, at Westminster.</title>
    <author>Albemarle, George Monck, Duke of, 1608-1670.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A76003 of text xxx in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason E1000_23). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 4 1-bit group-IV TIFF page images.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">A76003</idno>
    <idno type="STC">Wing A848</idno>
    <idno type="STC">Thomason E1000_23</idno>
    <idno type="STC">ESTC R1188</idno>
    <idno type="EEBO-CITATION">99859159</idno>
    <idno type="PROQUEST">99859159</idno>
    <idno type="VID">111226</idno>
    <idno type="PROQUESTGOID">2240868492</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online text creation partnership.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. A76003)</note>
    <note>Transcribed from: (Early English Books Online ; image set 111226)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 148:E1000[23])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>A letter from General Monck from Dalkeith, 13 October 1659. Directed as followeth. For the Right Honorable William Lenthal, Esquire, Speaker; to be communicated to the Parliament of the Common-wealth of England, at Westminster.</title>
      <author>Albemarle, George Monck, Duke of, 1608-1670.</author>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>6 p.</extent>
     <publicationStmt>
      <publisher>[s.n.],</publisher>
      <pubPlace>London :</pubPlace>
      <date>printed, an. Dom. 1659.</date>
     </publicationStmt>
     <notesStmt>
      <note>Annotation on Thomason copy: "8ber [i.e. October] 22".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>A letter from General Monck from Dalkeith, 13 October 1659. :$bDirected as followeth. For the Right Honorable William Lenthal, Esquire, Speaker; to be</ep:title>
    <ep:author>[no entry]</ep:author>
    <ep:publicationYear>1659</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>7</ep:pageCount>
    <ep:wordCount>481</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2013-03</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2013-03</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2013-07</date>
    <label>John Pas</label>
        Sampled and proofread
      </change>
   <change>
    <date>2013-07</date>
    <label>John Pas</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2014-03</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A76003-e10010">
  <front xml:id="A76003-e10020">
   <div type="title_page" xml:id="A76003-e10030">
    <pb facs="tcp:111226:1" rend="simple:additions" xml:id="A76003-001-a"/>
    <p xml:id="A76003-e10040">
     <w lemma="a" pos="d" xml:id="A76003-001-a-0010">A</w>
     <w lemma="letter" pos="n1" xml:id="A76003-001-a-0020">LETTER</w>
     <w lemma="from" pos="acp" xml:id="A76003-001-a-0030">FROM</w>
     <w lemma="general" pos="n1" xml:id="A76003-001-a-0040">General</w>
     <w lemma="Monck" pos="nn1" xml:id="A76003-001-a-0050">Monck</w>
     <pc unit="sentence" xml:id="A76003-001-a-0060">.</pc>
     <w lemma="from" pos="acp" xml:id="A76003-001-a-0070">From</w>
     <w lemma="dalkeith" pos="nn1" rend="hi" xml:id="A76003-001-a-0080">Dalkeith</w>
     <pc xml:id="A76003-001-a-0090">,</pc>
     <w lemma="13" pos="crd" xml:id="A76003-001-a-0100">13</w>
     <w lemma="October" pos="nn1" rend="hi" xml:id="A76003-001-a-0110">October</w>
     <w lemma="1659." pos="crd" xml:id="A76003-001-a-0120">1659.</w>
     <pc unit="sentence" xml:id="A76003-001-a-0130"/>
     <w lemma="direct" pos="vvn" xml:id="A76003-001-a-0140">Directed</w>
     <w lemma="as" pos="acp" xml:id="A76003-001-a-0150">as</w>
     <w lemma="follow" pos="vvz" xml:id="A76003-001-a-0160">followeth</w>
     <pc unit="sentence" xml:id="A76003-001-a-0170">.</pc>
    </p>
    <p xml:id="A76003-e10070">
     <w lemma="for" pos="acp" xml:id="A76003-001-a-0180">For</w>
     <w lemma="the" pos="d" xml:id="A76003-001-a-0190">the</w>
     <w lemma="right" pos="n1-j" xml:id="A76003-001-a-0200">Right</w>
     <w lemma="honourable" pos="j" reg="Honourable" xml:id="A76003-001-a-0210">Honorable</w>
     <hi xml:id="A76003-e10080">
      <w lemma="William" pos="nn1" xml:id="A76003-001-a-0220">William</w>
      <w lemma="lenthal" pos="nn1" xml:id="A76003-001-a-0230">Lenthal</w>
     </hi>
     <pc rend="follows-hi" xml:id="A76003-001-a-0240">,</pc>
     <w lemma="esquire" pos="n1" xml:id="A76003-001-a-0250">Esquire</w>
     <pc xml:id="A76003-001-a-0260">,</pc>
     <w lemma="speaker" pos="n1" xml:id="A76003-001-a-0270">Speaker</w>
     <pc xml:id="A76003-001-a-0280">;</pc>
     <w lemma="to" pos="prt" xml:id="A76003-001-a-0290">To</w>
     <w lemma="be" pos="vvi" xml:id="A76003-001-a-0300">be</w>
     <w lemma="communicate" pos="vvn" xml:id="A76003-001-a-0310">Communicated</w>
     <w lemma="to" pos="acp" xml:id="A76003-001-a-0320">to</w>
     <w lemma="the" pos="d" xml:id="A76003-001-a-0330">the</w>
     <w lemma="parliament" pos="n1" xml:id="A76003-001-a-0340">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A76003-001-a-0350">of</w>
     <w lemma="the" pos="d" xml:id="A76003-001-a-0360">the</w>
     <w lemma="commonwealth" pos="n1" reg="Commonwealth" xml:id="A76003-001-a-0370">Common-wealth</w>
     <w lemma="of" pos="acp" xml:id="A76003-001-a-0380">of</w>
     <w lemma="England" pos="nn1" rend="hi" xml:id="A76003-001-a-0390">England</w>
     <pc xml:id="A76003-001-a-0400">,</pc>
     <w lemma="at" pos="acp" xml:id="A76003-001-a-0410">at</w>
     <w lemma="Westminster" pos="nn1" rend="hi" xml:id="A76003-001-a-0420">Westminster</w>
     <pc unit="sentence" xml:id="A76003-001-a-0430">.</pc>
    </p>
    <p xml:id="A76003-e10110">
     <w lemma="London" pos="nn1" rend="hi" xml:id="A76003-001-a-0440">London</w>
     <pc xml:id="A76003-001-a-0450">,</pc>
     <w lemma="print" pos="vvn" xml:id="A76003-001-a-0460">Printed</w>
     <pc xml:id="A76003-001-a-0470">,</pc>
     <hi xml:id="A76003-e10130">
      <w lemma="an." pos="ab" xml:id="A76003-001-a-0480">An.</w>
      <w lemma="dom." pos="ab" xml:id="A76003-001-a-0490">Dom.</w>
     </hi>
     <w lemma="1659." pos="crd" xml:id="A76003-001-a-0500">1659.</w>
     <pc unit="sentence" xml:id="A76003-001-a-0510"/>
    </p>
   </div>
  </front>
  <body xml:id="A76003-e10140">
   <div type="letter" xml:id="A76003-e10150">
    <pb facs="tcp:111226:2" xml:id="A76003-002-a"/>
    <pb facs="tcp:111226:2" n="3" xml:id="A76003-002-b"/>
    <head xml:id="A76003-e10160">
     <w lemma="for" pos="acp" xml:id="A76003-002-b-0010">For</w>
     <w lemma="the" pos="d" xml:id="A76003-002-b-0020">the</w>
     <w lemma="right" pos="n1-j" xml:id="A76003-002-b-0030">Right</w>
     <w lemma="honourable" pos="j" reg="Honourable" xml:id="A76003-002-b-0040">Honorable</w>
     <hi xml:id="A76003-e10170">
      <w lemma="William" pos="nn1" xml:id="A76003-002-b-0050">William</w>
      <w lemma="lenthall" pos="nn1" xml:id="A76003-002-b-0060">Lenthall</w>
      <pc xml:id="A76003-002-b-0070">,</pc>
      <w lemma="esquire" pos="n1" xml:id="A76003-002-b-0080">Esquire</w>
     </hi>
     <pc rend="follows-hi" xml:id="A76003-002-b-0090">,</pc>
     <w lemma="speaker" pos="n1" xml:id="A76003-002-b-0100">Speaker</w>
     <pc xml:id="A76003-002-b-0110">;</pc>
     <w lemma="to" pos="prt" xml:id="A76003-002-b-0120">To</w>
     <w lemma="be" pos="vvi" xml:id="A76003-002-b-0130">be</w>
     <w lemma="communicate" pos="vvn" xml:id="A76003-002-b-0140">Communicated</w>
     <w lemma="to" pos="acp" xml:id="A76003-002-b-0150">to</w>
     <w lemma="the" pos="d" xml:id="A76003-002-b-0160">the</w>
     <w lemma="parliament" pos="n1" xml:id="A76003-002-b-0170">Parliament</w>
     <w lemma="of" pos="acp" xml:id="A76003-002-b-0180">of</w>
     <w lemma="the" pos="d" xml:id="A76003-002-b-0190">the</w>
     <w lemma="commonwealth" pos="n1" reg="Commonwealth" xml:id="A76003-002-b-0200">Common-wealth</w>
     <w lemma="of" pos="acp" xml:id="A76003-002-b-0210">of</w>
     <w lemma="England" pos="nn1" xml:id="A76003-002-b-0220">England</w>
     <pc xml:id="A76003-002-b-0230">,</pc>
     <w lemma="at" pos="acp" xml:id="A76003-002-b-0240">at</w>
     <w lemma="Westminster" pos="nn1" xml:id="A76003-002-b-0250">Westminster</w>
     <pc unit="sentence" xml:id="A76003-002-b-0260">.</pc>
    </head>
    <opener xml:id="A76003-e10180">
     <salute xml:id="A76003-e10190">
      <w lemma="right" pos="av-j" xml:id="A76003-002-b-0270">Right</w>
      <w lemma="honourable" pos="j" xml:id="A76003-002-b-0280">Honourable</w>
      <pc xml:id="A76003-002-b-0290">,</pc>
     </salute>
    </opener>
    <p xml:id="A76003-e10200">
     <w lemma="I" pos="pns" xml:id="A76003-002-b-0300">I</w>
     <w lemma="receive" pos="vvd" xml:id="A76003-002-b-0310">Received</w>
     <w lemma="you" pos="png" xml:id="A76003-002-b-0320">Yours</w>
     <w lemma="of" pos="acp" xml:id="A76003-002-b-0330">of</w>
     <w lemma="the" pos="d" xml:id="A76003-002-b-0340">the</w>
     <w lemma="7" orig="7ᵗʰ" pos="ord" xml:id="A76003-002-b-0350">7th</w>
     <w lemma="instant" pos="j" xml:id="A76003-002-b-0370">Instant</w>
     <pc xml:id="A76003-002-b-0380">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-002-b-0390">and</w>
     <w lemma="can" pos="vmbx" xml:id="A76003-002-b-0400">cannot</w>
     <w lemma="but" pos="acp" xml:id="A76003-002-b-0410">but</w>
     <w lemma="with" pos="acp" xml:id="A76003-002-b-0420">with</w>
     <w lemma="thankfulness" pos="n1" xml:id="A76003-002-b-0430">thankfulness</w>
     <w lemma="acknowledge" pos="vvb" xml:id="A76003-002-b-0440">Acknowledge</w>
     <w lemma="the" pos="d" xml:id="A76003-002-b-0450">the</w>
     <w lemma="great" pos="j" xml:id="A76003-002-b-0460">Great</w>
     <w lemma="grace" pos="n1" xml:id="A76003-002-b-0470">Grace</w>
     <w lemma="and" pos="cc" xml:id="A76003-002-b-0480">and</w>
     <w lemma="favour" pos="n1" xml:id="A76003-002-b-0490">Favour</w>
     <w lemma="the" pos="d" xml:id="A76003-002-b-0500">the</w>
     <w lemma="parliament" pos="n1" xml:id="A76003-002-b-0510">Parliament</w>
     <w lemma="be" pos="vvb" xml:id="A76003-002-b-0520">are</w>
     <w lemma="please" pos="vvn" xml:id="A76003-002-b-0530">pleased</w>
     <w lemma="to" pos="prt" xml:id="A76003-002-b-0540">to</w>
     <w lemma="vouchsafe" pos="vvi" xml:id="A76003-002-b-0550">vouchsafe</w>
     <w lemma="to" pos="acp" xml:id="A76003-002-b-0560">to</w>
     <w lemma="i" pos="pno" xml:id="A76003-002-b-0570">me</w>
     <pc xml:id="A76003-002-b-0580">,</pc>
     <w lemma="in" pos="acp" xml:id="A76003-002-b-0590">in</w>
     <w lemma="take" pos="vvg" xml:id="A76003-002-b-0600">taking</w>
     <w lemma="notice" pos="n1" xml:id="A76003-002-b-0610">Notice</w>
     <w lemma="of" pos="acp" xml:id="A76003-002-b-0620">of</w>
     <w lemma="my" pos="po" xml:id="A76003-002-b-0630">my</w>
     <w lemma="weak" pos="j" xml:id="A76003-002-b-0640">weak</w>
     <w lemma="and" pos="cc" xml:id="A76003-002-b-0650">and</w>
     <w lemma="worthless" pos="j" xml:id="A76003-002-b-0660">worthless</w>
     <w lemma="endeavour" pos="n2" xml:id="A76003-002-b-0670">Endeavours</w>
     <w lemma="in" pos="acp" xml:id="A76003-002-b-0680">in</w>
     <w lemma="their" pos="po" xml:id="A76003-002-b-0690">their</w>
     <w lemma="service" pos="n1" xml:id="A76003-002-b-0700">Service</w>
     <pc unit="sentence" xml:id="A76003-002-b-0710">.</pc>
     <w lemma="I" pos="pns" xml:id="A76003-002-b-0720">I</w>
     <w lemma="confess" pos="vvb" xml:id="A76003-002-b-0730">confess</w>
     <w lemma="such" pos="d" xml:id="A76003-002-b-0740">such</w>
     <w lemma="encouragement" pos="n1" xml:id="A76003-002-b-0750">Encouragement</w>
     <w lemma="be" pos="vvz" xml:id="A76003-002-b-0760">is</w>
     <w lemma="sufficient" pos="j" xml:id="A76003-002-b-0770">sufficient</w>
     <pb facs="tcp:111226:3" n="4" xml:id="A76003-003-a"/>
     <w lemma="to" pos="prt" xml:id="A76003-003-a-0010">to</w>
     <w lemma="reward" pos="vvi" xml:id="A76003-003-a-0020">Reward</w>
     <w lemma="the" pos="d" xml:id="A76003-003-a-0030">the</w>
     <w lemma="high" pos="js" xml:id="A76003-003-a-0040">Highest</w>
     <w lemma="merit" pos="n2" xml:id="A76003-003-a-0050">Merits</w>
     <pc xml:id="A76003-003-a-0060">;</pc>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-0070">I</w>
     <w lemma="hope" pos="vvb" xml:id="A76003-003-a-0080">hope</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-0090">I</w>
     <w lemma="shall" pos="vmb" xml:id="A76003-003-a-0100">shall</w>
     <w lemma="make" pos="vvi" xml:id="A76003-003-a-0110">make</w>
     <w lemma="such" pos="d" xml:id="A76003-003-a-0120">such</w>
     <w lemma="use" pos="n1" xml:id="A76003-003-a-0130">Use</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-a-0140">of</w>
     <w lemma="it" pos="pn" xml:id="A76003-003-a-0150">It</w>
     <pc xml:id="A76003-003-a-0160">,</pc>
     <w lemma="not" pos="xx" xml:id="A76003-003-a-0170">not</w>
     <w lemma="only" pos="av-j" xml:id="A76003-003-a-0180">only</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-a-0190">to</w>
     <w lemma="satisfy" pos="vvi" reg="satisfy" xml:id="A76003-003-a-0200">satisfie</w>
     <w lemma="myself" pos="pr" reg="myself" xml:id="A76003-003-a-0210">my self</w>
     <w lemma="as" pos="acp" xml:id="A76003-003-a-0230">as</w>
     <w lemma="the" pos="d" xml:id="A76003-003-a-0240">the</w>
     <w lemma="best" pos="js" xml:id="A76003-003-a-0250">best</w>
     <w lemma="recompense" pos="n1" reg="Recompense" xml:id="A76003-003-a-0260">Recompence</w>
     <w lemma="for" pos="acp" xml:id="A76003-003-a-0270">for</w>
     <w lemma="my" pos="po" xml:id="A76003-003-a-0280">my</w>
     <w lemma="former" pos="j" xml:id="A76003-003-a-0290">former</w>
     <w lemma="poor" pos="j" xml:id="A76003-003-a-0300">poor</w>
     <w lemma="service" pos="n2" xml:id="A76003-003-a-0310">Services</w>
     <pc xml:id="A76003-003-a-0320">,</pc>
     <w lemma="but" pos="acp" xml:id="A76003-003-a-0330">but</w>
     <w lemma="as" pos="acp" xml:id="A76003-003-a-0340">as</w>
     <w lemma="a" pos="d" xml:id="A76003-003-a-0350">a</w>
     <w lemma="motive" pos="n1" xml:id="A76003-003-a-0360">Motive</w>
     <w lemma="to" pos="acp" xml:id="A76003-003-a-0370">to</w>
     <w lemma="future" pos="j" xml:id="A76003-003-a-0380">Future</w>
     <w lemma="obedience" pos="n1" xml:id="A76003-003-a-0390">Obedience</w>
     <w lemma="and" pos="cc" xml:id="A76003-003-a-0400">and</w>
     <w lemma="loyalty" pos="n1" xml:id="A76003-003-a-0410">Loyalty</w>
     <w lemma="to" pos="acp" xml:id="A76003-003-a-0420">to</w>
     <w lemma="they" pos="pno" xml:id="A76003-003-a-0430">them</w>
     <pc xml:id="A76003-003-a-0440">:</pc>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-0450">I</w>
     <w lemma="bless" pos="vvb" xml:id="A76003-003-a-0460">bless</w>
     <w lemma="the" pos="d" xml:id="A76003-003-a-0470">the</w>
     <w lemma="lord" pos="n1" xml:id="A76003-003-a-0480">Lord</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-0490">I</w>
     <w lemma="have" pos="vvb" xml:id="A76003-003-a-0500">have</w>
     <w lemma="a" pos="d" xml:id="A76003-003-a-0510">a</w>
     <w lemma="witness" pos="n1" xml:id="A76003-003-a-0520">Witness</w>
     <w lemma="in" pos="acp" xml:id="A76003-003-a-0530">in</w>
     <w lemma="my" pos="po" xml:id="A76003-003-a-0540">mine</w>
     <w lemma="own" pos="d" xml:id="A76003-003-a-0550">own</w>
     <w lemma="heart" pos="n1" xml:id="A76003-003-a-0560">Heart</w>
     <pc xml:id="A76003-003-a-0570">,</pc>
     <w lemma="that" pos="cs" xml:id="A76003-003-a-0580">That</w>
     <w lemma="my" pos="po" xml:id="A76003-003-a-0590">my</w>
     <w lemma="design" pos="n2" xml:id="A76003-003-a-0600">Designs</w>
     <w lemma="tend" pos="vvb" xml:id="A76003-003-a-0610">tend</w>
     <w lemma="not" pos="xx" xml:id="A76003-003-a-0620">not</w>
     <w lemma="to" pos="acp" xml:id="A76003-003-a-0630">to</w>
     <w lemma="any" pos="d" xml:id="A76003-003-a-0640">any</w>
     <w lemma="other" pos="d" xml:id="A76003-003-a-0650">other</w>
     <w lemma="end" pos="n1" xml:id="A76003-003-a-0660">End</w>
     <w lemma="than" pos="cs" xml:id="A76003-003-a-0670">than</w>
     <w lemma="my" pos="po" xml:id="A76003-003-a-0680">my</w>
     <w lemma="country" pos="n2" xml:id="A76003-003-a-0690">Countries</w>
     <w lemma="good" pos="j" xml:id="A76003-003-a-0700">Good</w>
     <pc xml:id="A76003-003-a-0710">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-a-0720">and</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-0730">I</w>
     <w lemma="shall" pos="vmb" xml:id="A76003-003-a-0740">shall</w>
     <w lemma="with" pos="acp" xml:id="A76003-003-a-0750">with</w>
     <w lemma="more" pos="dc" xml:id="A76003-003-a-0760">more</w>
     <w lemma="cheerfulness" pos="n1" xml:id="A76003-003-a-0770">Cheerfulness</w>
     <w lemma="return" pos="vvi" xml:id="A76003-003-a-0780">return</w>
     <w lemma="the" pos="d" xml:id="A76003-003-a-0790">the</w>
     <w lemma="sword" pos="n1" xml:id="A76003-003-a-0800">Sword</w>
     <w lemma="into" pos="acp" xml:id="A76003-003-a-0810">into</w>
     <w lemma="your" pos="po" xml:id="A76003-003-a-0820">Your</w>
     <w lemma="hand" pos="n2" xml:id="A76003-003-a-0830">Hands</w>
     <pc xml:id="A76003-003-a-0840">,</pc>
     <w lemma="than" pos="cs" xml:id="A76003-003-a-0850">than</w>
     <w lemma="ever" pos="av" xml:id="A76003-003-a-0860">ever</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-0870">I</w>
     <w lemma="receive" pos="vvd" reg="received" xml:id="A76003-003-a-0880">receiv'd</w>
     <w lemma="it" pos="pn" xml:id="A76003-003-a-0890">it</w>
     <w lemma="with" pos="acp" xml:id="A76003-003-a-0900">with</w>
     <pc xml:id="A76003-003-a-0910">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-a-0920">and</w>
     <w lemma="desire" pos="vvb" xml:id="A76003-003-a-0930">desire</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-a-0940">to</w>
     <w lemma="attend" pos="vvi" xml:id="A76003-003-a-0950">attend</w>
     <w lemma="your" pos="po" xml:id="A76003-003-a-0960">Your</w>
     <w lemma="pleasure" pos="n1" xml:id="A76003-003-a-0970">Pleasure</w>
     <w lemma="if" pos="cs" xml:id="A76003-003-a-0980">if</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-a-0990">You</w>
     <w lemma="shall" pos="vmb" xml:id="A76003-003-a-1000">shall</w>
     <w lemma="have" pos="vvi" xml:id="A76003-003-a-1010">have</w>
     <w lemma="no" pos="avx-d" xml:id="A76003-003-a-1020">no</w>
     <w lemma="further" pos="avc-j" xml:id="A76003-003-a-1030">further</w>
     <w lemma="use" pos="vvi" xml:id="A76003-003-a-1040">Use</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-a-1050">of</w>
     <w lemma="my" pos="po" xml:id="A76003-003-a-1060">my</w>
     <w lemma="service" pos="n1" xml:id="A76003-003-a-1070">Service</w>
     <pc xml:id="A76003-003-a-1080">:</pc>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-1090">I</w>
     <w lemma="shall" pos="vmb" xml:id="A76003-003-a-1100">shall</w>
     <w lemma="give" pos="vvi" xml:id="A76003-003-a-1110">give</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-a-1120">You</w>
     <w lemma="the" pos="d" xml:id="A76003-003-a-1130">the</w>
     <w lemma="best" pos="js" xml:id="A76003-003-a-1140">best</w>
     <w lemma="account" pos="n1" xml:id="A76003-003-a-1150">Account</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-1160">I</w>
     <w lemma="can" pos="vmb" xml:id="A76003-003-a-1170">can</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-a-1180">of</w>
     <w lemma="the" pos="d" xml:id="A76003-003-a-1190">the</w>
     <w lemma="force" pos="n2" xml:id="A76003-003-a-1200">Forces</w>
     <w lemma="here" pos="av" xml:id="A76003-003-a-1210">here</w>
     <pc xml:id="A76003-003-a-1220">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-a-1230">and</w>
     <w lemma="endeavour" pos="vvi" xml:id="A76003-003-a-1240">endeavour</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-a-1250">to</w>
     <w lemma="keep" pos="vvi" xml:id="A76003-003-a-1260">keep</w>
     <w lemma="they" pos="pno" xml:id="A76003-003-a-1270">them</w>
     <w lemma="in" pos="acp" xml:id="A76003-003-a-1280">in</w>
     <w lemma="due" pos="j" xml:id="A76003-003-a-1290">due</w>
     <w lemma="obedience" pos="n1" xml:id="A76003-003-a-1300">Obedience</w>
     <w lemma="to" pos="acp" xml:id="A76003-003-a-1310">to</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-a-1320">You</w>
     <pc xml:id="A76003-003-a-1330">,</pc>
     <pc join="right" xml:id="A76003-003-a-1340">(</pc>
     <w lemma="but" pos="acp" xml:id="A76003-003-a-1350">but</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-1360">I</w>
     <w lemma="can" pos="vmbx" xml:id="A76003-003-a-1370">cannot</w>
     <w lemma="undertake" pos="vvi" xml:id="A76003-003-a-1380">undertake</w>
     <w lemma="for" pos="acp" xml:id="A76003-003-a-1390">for</w>
     <w lemma="man" pos="n2" xml:id="A76003-003-a-1400">men</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-a-1410">of</w>
     <w lemma="some" pos="d" xml:id="A76003-003-a-1420">some</w>
     <w lemma="spirit" pos="n2" xml:id="A76003-003-a-1430">Spirits</w>
     <pc xml:id="A76003-003-a-1440">.</pc>
     <pc unit="sentence" xml:id="A76003-003-a-1450">)</pc>
     <w lemma="I" pos="pns" xml:id="A76003-003-a-1460">I</w>
     <w lemma="shall" pos="vmb" reg="shall" xml:id="A76003-003-a-1470">shal</w>
     <w lemma="not" pos="xx" xml:id="A76003-003-a-1480">not</w>
     <w lemma="trouble" pos="vvi" xml:id="A76003-003-a-1490">trouble</w>
     <w lemma="your" pos="po" xml:id="A76003-003-a-1500">your</w>
     <pb facs="tcp:111226:3" n="5" xml:id="A76003-003-b"/>
     <w lemma="council" pos="n2" xml:id="A76003-003-b-0010">Councils</w>
     <w lemma="with" pos="acp" xml:id="A76003-003-b-0020">with</w>
     <w lemma="my" pos="po" xml:id="A76003-003-b-0030">my</w>
     <w lemma="impertinency" pos="n2" xml:id="A76003-003-b-0040">Impertinencies</w>
     <w lemma="any" pos="d" xml:id="A76003-003-b-0050">any</w>
     <w lemma="further" pos="jc" xml:id="A76003-003-b-0060">further</w>
     <pc xml:id="A76003-003-b-0070">,</pc>
     <w lemma="but" pos="acp" xml:id="A76003-003-b-0080">but</w>
     <w lemma="entreat" pos="vvb" reg="Entreat" xml:id="A76003-003-b-0090">Intreat</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-0100">You</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-b-0110">to</w>
     <w lemma="give" pos="vvi" xml:id="A76003-003-b-0120">give</w>
     <w lemma="i" pos="pno" xml:id="A76003-003-b-0130">me</w>
     <w lemma="leave" pos="vvi" xml:id="A76003-003-b-0140">leave</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-b-0150">to</w>
     <w lemma="mind" pos="vvi" xml:id="A76003-003-b-0160">mind</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-0170">You</w>
     <pc join="right" xml:id="A76003-003-b-0180">(</pc>
     <w lemma="what" pos="crq" xml:id="A76003-003-b-0190">what</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-b-0200">I</w>
     <w lemma="know" pos="vvb" xml:id="A76003-003-b-0210">know</w>
     <w lemma="be" pos="vvz" xml:id="A76003-003-b-0220">is</w>
     <w lemma="much" pos="av-d" xml:id="A76003-003-b-0230">much</w>
     <w lemma="upon" pos="acp" xml:id="A76003-003-b-0240">upon</w>
     <w lemma="your" pos="po" xml:id="A76003-003-b-0250">Your</w>
     <w lemma="heart" pos="n2" xml:id="A76003-003-b-0260">Hearts</w>
     <w lemma="already" pos="av" xml:id="A76003-003-b-0270">already</w>
     <pc xml:id="A76003-003-b-0280">)</pc>
     <w lemma="that" pos="cs" xml:id="A76003-003-b-0290">That</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-0300">You</w>
     <w lemma="will" pos="vmd" xml:id="A76003-003-b-0310">would</w>
     <w lemma="be" pos="vvi" xml:id="A76003-003-b-0320">be</w>
     <w lemma="please" pos="vvn" xml:id="A76003-003-b-0330">pleased</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-b-0340">to</w>
     <w lemma="hasten" pos="vvi" xml:id="A76003-003-b-0350">hasten</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-0360">the</w>
     <w lemma="settlement" pos="n1" xml:id="A76003-003-b-0370">Settlement</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-b-0380">of</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-0390">the</w>
     <w lemma="government" pos="n1" xml:id="A76003-003-b-0400">Government</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-b-0410">of</w>
     <w lemma="these" pos="d" xml:id="A76003-003-b-0420">these</w>
     <w lemma="nation" pos="n2" xml:id="A76003-003-b-0430">Nations</w>
     <w lemma="in" pos="acp" xml:id="A76003-003-b-0440">in</w>
     <w lemma="a" pos="d" xml:id="A76003-003-b-0450">a</w>
     <w lemma="commonwealth" pos="n1" xml:id="A76003-003-b-0460">Commonwealth</w>
     <w lemma="way" pos="n1" xml:id="A76003-003-b-0470">Way</w>
     <pc xml:id="A76003-003-b-0480">,</pc>
     <w lemma="in" pos="acp" xml:id="A76003-003-b-0490">in</w>
     <w lemma="successive" pos="j" xml:id="A76003-003-b-0500">Successive</w>
     <w lemma="parliament" pos="n2" xml:id="A76003-003-b-0510">Parliaments</w>
     <pc xml:id="A76003-003-b-0520">,</pc>
     <w lemma="so" pos="av" xml:id="A76003-003-b-0530">so</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-b-0540">to</w>
     <w lemma="be" pos="vvi" xml:id="A76003-003-b-0550">be</w>
     <w lemma="regulate" pos="vvn" xml:id="A76003-003-b-0560">Regulated</w>
     <w lemma="in" pos="acp" xml:id="A76003-003-b-0570">in</w>
     <w lemma="election" pos="n2" xml:id="A76003-003-b-0580">Elections</w>
     <w lemma="as" pos="acp" xml:id="A76003-003-b-0590">as</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-0600">You</w>
     <w lemma="shall" pos="vmb" xml:id="A76003-003-b-0610">shall</w>
     <w lemma="think" pos="vvi" xml:id="A76003-003-b-0620">think</w>
     <w lemma="fit" pos="j" xml:id="A76003-003-b-0630">fit</w>
     <pc xml:id="A76003-003-b-0640">;</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-b-0650">and</w>
     <w lemma="that" pos="cs" xml:id="A76003-003-b-0660">that</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-0670">You</w>
     <w lemma="will" pos="vmb" xml:id="A76003-003-b-0680">will</w>
     <w lemma="increase" pos="vvi" xml:id="A76003-003-b-0690">increase</w>
     <w lemma="your" pos="po" xml:id="A76003-003-b-0700">your</w>
     <w lemma="favour" pos="n1" xml:id="A76003-003-b-0710">Favour</w>
     <w lemma="to" pos="acp" xml:id="A76003-003-b-0720">to</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-0730">the</w>
     <w lemma="minister" pos="n2" xml:id="A76003-003-b-0740">Ministers</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-b-0750">of</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-0760">the</w>
     <w lemma="gospel" pos="n1" xml:id="A76003-003-b-0770">Gospel</w>
     <pc xml:id="A76003-003-b-0780">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-b-0790">and</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-0800">the</w>
     <w lemma="sober" pos="j" xml:id="A76003-003-b-0810">Sober</w>
     <w lemma="professor" pos="n2" reg="Professors" xml:id="A76003-003-b-0820">Professours</w>
     <w lemma="thereof" pos="av" xml:id="A76003-003-b-0830">thereof</w>
     <pc xml:id="A76003-003-b-0840">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-b-0850">and</w>
     <w lemma="God" pos="nn1" xml:id="A76003-003-b-0860">God</w>
     <w lemma="will" pos="vmb" xml:id="A76003-003-b-0870">will</w>
     <w lemma="be" pos="vvi" xml:id="A76003-003-b-0880">be</w>
     <w lemma="in" pos="acp" xml:id="A76003-003-b-0890">in</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-0900">the</w>
     <w lemma="midst" pos="n1" xml:id="A76003-003-b-0910">midst</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-b-0920">of</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-0930">you</w>
     <pc xml:id="A76003-003-b-0940">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-b-0950">and</w>
     <w lemma="bless" pos="vvb" xml:id="A76003-003-b-0960">bless</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-0970">you</w>
     <pc xml:id="A76003-003-b-0980">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-b-0990">and</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-1000">you</w>
     <w lemma="need" pos="vvb" xml:id="A76003-003-b-1010">need</w>
     <w lemma="not" pos="xx" xml:id="A76003-003-b-1020">not</w>
     <w lemma="doubt" pos="vvi" xml:id="A76003-003-b-1030">doubt</w>
     <w lemma="but" pos="acp" xml:id="A76003-003-b-1040">but</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-1050">the</w>
     <w lemma="heart" pos="n2" xml:id="A76003-003-b-1060">Hearts</w>
     <w lemma="and" pos="cc" xml:id="A76003-003-b-1070">and</w>
     <w lemma="hand" pos="n2" xml:id="A76003-003-b-1080">Hands</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-b-1090">of</w>
     <w lemma="all" pos="d" xml:id="A76003-003-b-1100">all</w>
     <w lemma="good" pos="j" xml:id="A76003-003-b-1110">good</w>
     <w lemma="man" pos="n2" xml:id="A76003-003-b-1120">men</w>
     <w lemma="will" pos="vmb" xml:id="A76003-003-b-1130">will</w>
     <w lemma="be" pos="vvi" xml:id="A76003-003-b-1140">be</w>
     <w lemma="with" pos="acp" xml:id="A76003-003-b-1150">with</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-1160">you</w>
     <pc xml:id="A76003-003-b-1170">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-003-b-1180">and</w>
     <w lemma="for" pos="acp" xml:id="A76003-003-b-1190">for</w>
     <w lemma="myself" pos="pr" reg="myself" xml:id="A76003-003-b-1200">my self</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-b-1220">I</w>
     <w lemma="desire" pos="vvb" xml:id="A76003-003-b-1230">desire</w>
     <pc xml:id="A76003-003-b-1240">,</pc>
     <w lemma="that" pos="cs" xml:id="A76003-003-b-1250">That</w>
     <w lemma="you" pos="pn" xml:id="A76003-003-b-1260">you</w>
     <w lemma="will" pos="vmd" xml:id="A76003-003-b-1270">would</w>
     <w lemma="be" pos="vvi" xml:id="A76003-003-b-1280">be</w>
     <w lemma="assure" pos="vvn" xml:id="A76003-003-b-1290">assured</w>
     <w lemma="that" pos="cs" xml:id="A76003-003-b-1300">that</w>
     <w lemma="I" pos="pns" xml:id="A76003-003-b-1310">I</w>
     <w lemma="do" pos="vvb" xml:id="A76003-003-b-1320">do</w>
     <w lemma="not" pos="xx" xml:id="A76003-003-b-1330">not</w>
     <w lemma="think" pos="vvi" xml:id="A76003-003-b-1340">think</w>
     <w lemma="my" pos="po" xml:id="A76003-003-b-1350">my</w>
     <w lemma="life" pos="n1" xml:id="A76003-003-b-1360">life</w>
     <w lemma="too" pos="av" xml:id="A76003-003-b-1370">too</w>
     <w lemma="precious" pos="j" xml:id="A76003-003-b-1380">precious</w>
     <w lemma="to" pos="prt" xml:id="A76003-003-b-1390">to</w>
     <w lemma="hazard" pos="vvi" xml:id="A76003-003-b-1400">hazard</w>
     <w lemma="in" pos="acp" xml:id="A76003-003-b-1410">in</w>
     <w lemma="the" pos="d" xml:id="A76003-003-b-1420">the</w>
     <w lemma="defence" pos="n1" xml:id="A76003-003-b-1430">Defence</w>
     <w lemma="of" pos="acp" xml:id="A76003-003-b-1440">of</w>
     <pb facs="tcp:111226:4" n="6" xml:id="A76003-004-a"/>
     <hi xml:id="A76003-e10220">
      <w lemma="the" pos="d" xml:id="A76003-004-a-0010">The</w>
      <w lemma="supreme" pos="j" reg="Supreme" xml:id="A76003-004-a-0020">Supream</w>
      <w lemma="authority" pos="n1" xml:id="A76003-004-a-0030">Authority</w>
      <pc xml:id="A76003-004-a-0040">,</pc>
      <w lemma="the" pos="d" xml:id="A76003-004-a-0050">the</w>
      <w lemma="parliament" pos="n1" xml:id="A76003-004-a-0060">Parliament</w>
      <w lemma="of" pos="acp" xml:id="A76003-004-a-0070">of</w>
     </hi>
     <w lemma="England" pos="nn1" xml:id="A76003-004-a-0080">England</w>
     <pc unit="sentence" xml:id="A76003-004-a-0090">.</pc>
     <w lemma="the" pos="d" xml:id="A76003-004-a-0100">The</w>
     <w lemma="lord" pos="n1" xml:id="A76003-004-a-0110">Lord</w>
     <w lemma="bless" pos="vvb" xml:id="A76003-004-a-0120">bless</w>
     <w lemma="your" pos="po" xml:id="A76003-004-a-0130">your</w>
     <w lemma="council" pos="n2" xml:id="A76003-004-a-0140">Councils</w>
     <w lemma="with" pos="acp" xml:id="A76003-004-a-0150">with</w>
     <w lemma="peace" pos="n1" xml:id="A76003-004-a-0160">Peace</w>
     <w lemma="and" pos="cc" xml:id="A76003-004-a-0170">and</w>
     <w lemma="success" pos="n1" xml:id="A76003-004-a-0180">Success</w>
     <pc xml:id="A76003-004-a-0190">,</pc>
     <w lemma="and" pos="cc" xml:id="A76003-004-a-0200">and</w>
     <w lemma="make" pos="vvi" xml:id="A76003-004-a-0210">make</w>
     <w lemma="you" pos="pn" xml:id="A76003-004-a-0220">you</w>
     <w lemma="a" pos="d" xml:id="A76003-004-a-0230">a</w>
     <w lemma="terror" pos="n1" reg="Terror" xml:id="A76003-004-a-0240">Terrour</w>
     <w lemma="to" pos="acp" xml:id="A76003-004-a-0250">to</w>
     <w lemma="the" pos="d" xml:id="A76003-004-a-0260">the</w>
     <w lemma="nation" pos="n2" xml:id="A76003-004-a-0270">Nations</w>
     <w lemma="round" pos="av-j" xml:id="A76003-004-a-0280">round</w>
     <w lemma="about" pos="acp" xml:id="A76003-004-a-0290">about</w>
     <w lemma="you" pos="pn" xml:id="A76003-004-a-0300">you</w>
     <pc xml:id="A76003-004-a-0310">,</pc>
     <w lemma="which" pos="crq" xml:id="A76003-004-a-0320">which</w>
     <w lemma="be" pos="vvz" xml:id="A76003-004-a-0330">is</w>
     <w lemma="the" pos="d" xml:id="A76003-004-a-0340">the</w>
     <w lemma="earnest" pos="j" xml:id="A76003-004-a-0350">Earnest</w>
     <w lemma="prayer" pos="n1" xml:id="A76003-004-a-0360">Prayer</w>
     <w lemma="of" pos="acp" xml:id="A76003-004-a-0370">of</w>
     <w lemma="he" pos="pno" xml:id="A76003-004-a-0380">Him</w>
     <pc xml:id="A76003-004-a-0390">,</pc>
     <w lemma="who" pos="crq" xml:id="A76003-004-a-0400">who</w>
     <w lemma="be" pos="vvz" xml:id="A76003-004-a-0410">is</w>
     <pc xml:id="A76003-004-a-0420">,</pc>
    </p>
    <closer xml:id="A76003-e10230">
     <signed xml:id="A76003-e10240">
      <w lemma="your" pos="po" xml:id="A76003-004-a-0430">Your</w>
      <w lemma="lord" pos="n1" reg="Lord" xml:id="A76003-004-a-0440">Lorpps</w>
      <w cert="medium" lemma="pps" pos="wd" rend="sup" xml:id="A76003-004-a-0450">pps</w>
      <pc xml:id="A76003-004-a-0460">.</pc>
      <w lemma="most" pos="ds" xml:id="A76003-004-a-0470">most</w>
      <w lemma="humble" pos="j" xml:id="A76003-004-a-0480">humble</w>
      <w lemma="and" pos="cc" xml:id="A76003-004-a-0490">and</w>
      <w lemma="faithful" pos="j" xml:id="A76003-004-a-0500">faithful</w>
      <w lemma="servant" pos="n1" xml:id="A76003-004-a-0510">Servant</w>
      <pc xml:id="A76003-004-a-0520">,</pc>
      <w lemma="George" pos="nn1" xml:id="A76003-004-a-0530">George</w>
      <w lemma="Monck" pos="nn1" xml:id="A76003-004-a-0540">Monck</w>
      <pc unit="sentence" xml:id="A76003-004-a-0550">.</pc>
     </signed>
     <dateline xml:id="A76003-e10260">
      <w lemma="dalkeith" pos="nn1" rend="hi" xml:id="A76003-004-a-0560">Dalkeith</w>
      <pc xml:id="A76003-004-a-0570">,</pc>
      <date xml:id="A76003-e10280">
       <w lemma="13." pos="crd" xml:id="A76003-004-a-0580">13.</w>
       <w lemma="octob." pos="ab" rend="hi" xml:id="A76003-004-a-0600">Octob.</w>
       <w lemma="1659." pos="crd" xml:id="A76003-004-a-0610">1659.</w>
       <pc unit="sentence" xml:id="A76003-004-a-0620"/>
      </date>
     </dateline>
    </closer>
    <pb facs="tcp:111226:4" xml:id="A76003-004-b"/>
   </div>
  </body>
 </text>
</TEI>

<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A76461">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To certaine noble and honorable persons of the Honorable House of Commons assembled in Parliament.</title>
    <author>Best, Paul, 1590?-1657.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A76461 of text R210570 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.10[76]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A76461</idno>
    <idno type="STC">Wing B2054</idno>
    <idno type="STC">Thomason 669.f.10[76]</idno>
    <idno type="STC">ESTC R210570</idno>
    <idno type="EEBO-CITATION">99869355</idno>
    <idno type="PROQUEST">99869355</idno>
    <idno type="VID">162611</idno>
    <idno type="PROQUESTGOID">2240949029</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A76461)</note>
    <note>Transcribed from: (Early English Books Online ; image set 162611)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f10[76])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To certaine noble and honorable persons of the Honorable House of Commons assembled in Parliament.</title>
      <author>Best, Paul, 1590?-1657.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1646]</date>
     </publicationStmt>
     <notesStmt>
      <note>Signed at end: Paul Best.</note>
      <note>Imprint from Wing.</note>
      <note>Annotation on Thomason copy: "Aug: 13 1646".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Prisoners -- Great Britain -- Early works to 1800.</term>
     <term>Great Britain -- History -- Civil War, 1642-1649 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A76461</ep:tcp>
    <ep:estc> R210570</ep:estc>
    <ep:stc> (Thomason 669.f.10[76]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>To certaine noble and honorable persons of the Honorable House of Commons assembled in Parliament.</ep:title>
    <ep:author>Best, Paul</ep:author>
    <ep:publicationYear>1646</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>438</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-07</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A76461-e10">
  <body xml:id="A76461-e20">
   <pb facs="tcp:162611:1" rend="simple:additions" xml:id="A76461-001-a"/>
   <div type="letter" xml:id="A76461-e30">
    <head xml:id="A76461-e40">
     <w lemma="to" pos="acp" xml:id="A76461-001-a-0010">TO</w>
     <w lemma="certain" pos="j" reg="certain" xml:id="A76461-001-a-0020">CERTAINE</w>
     <w lemma="noble" pos="j" xml:id="A76461-001-a-0030">NOBLE</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-0040">AND</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A76461-001-a-0050">HONORABLE</w>
     <w lemma="person" pos="n2" xml:id="A76461-001-a-0060">Persons</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-0070">of</w>
     <w lemma="the" pos="d" xml:id="A76461-001-a-0080">the</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A76461-001-a-0090">Honorable</w>
     <w lemma="house" pos="n1" xml:id="A76461-001-a-0100">House</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-0110">of</w>
     <w lemma="commons" pos="n2" xml:id="A76461-001-a-0120">Commons</w>
     <w lemma="assemble" pos="vvn" xml:id="A76461-001-a-0130">assembled</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-0140">in</w>
     <w lemma="parliament" pos="n1" xml:id="A76461-001-a-0150">Parliament</w>
     <pc unit="sentence" xml:id="A76461-001-a-0160">.</pc>
    </head>
    <opener xml:id="A76461-e50">
     <salute xml:id="A76461-e60">
      <w lemma="right" pos="av-j" xml:id="A76461-001-a-0170">RIght</w>
      <w lemma="honble" pos="ab" rend="sup-right,2" xml:id="A76461-001-a-0180">Honble:</w>
      <w lemma="and" pos="cc" xml:id="A76461-001-a-0210">and</w>
      <w lemma="noble" pos="j" xml:id="A76461-001-a-0220">noble</w>
      <w lemma="by" pos="acp" xml:id="A76461-001-a-0230">by</w>
      <w lemma="your" pos="po" xml:id="A76461-001-a-0240">your</w>
      <w lemma="birth" pos="n1" xml:id="A76461-001-a-0250">birth</w>
      <w lemma="and" pos="cc" xml:id="A76461-001-a-0260">and</w>
      <w lemma="breed" pos="n1-vg" xml:id="A76461-001-a-0270">breeding</w>
      <pc xml:id="A76461-001-a-0280">;</pc>
      <w lemma="your" pos="po" xml:id="A76461-001-a-0290">your</w>
      <w lemma="profession" pos="n1" xml:id="A76461-001-a-0300">profession</w>
      <w lemma="of" pos="acp" xml:id="A76461-001-a-0310">of</w>
      <w lemma="piety" pos="n1" reg="piety" rend="hi" xml:id="A76461-001-a-0320">pietie</w>
      <w lemma="and" pos="cc" xml:id="A76461-001-a-0330">and</w>
      <hi xml:id="A76461-e90">
       <w lemma="religion" pos="n1" xml:id="A76461-001-a-0340">religion</w>
       <pc xml:id="A76461-001-a-0350">;</pc>
      </hi>
      <w lemma="your" pos="po" xml:id="A76461-001-a-0360">Your</w>
      <w lemma="employment" pos="n1" reg="employment" rend="hi" xml:id="A76461-001-a-0370">imployment</w>
      <w lemma="and" pos="cc" xml:id="A76461-001-a-0380">and</w>
      <hi xml:id="A76461-e110">
       <w lemma="interest" pos="n1" xml:id="A76461-001-a-0390">interest</w>
       <pc xml:id="A76461-001-a-0400">,</pc>
      </hi>
      <w lemma="furnish" pos="vvg" xml:id="A76461-001-a-0410">furnishing</w>
      <w lemma="you" pos="pn" xml:id="A76461-001-a-0420">you</w>
      <w lemma="with" pos="acp" xml:id="A76461-001-a-0430">with</w>
      <w lemma="occasion" pos="n1" xml:id="A76461-001-a-0440">occasion</w>
      <w lemma="and" pos="cc" xml:id="A76461-001-a-0450">and</w>
      <w lemma="power" pos="n1" xml:id="A76461-001-a-0460">power</w>
      <w lemma="of" pos="acp" xml:id="A76461-001-a-0470">of</w>
      <w lemma="exercise" pos="vvg" xml:id="A76461-001-a-0480">exercising</w>
      <w lemma="those" pos="d" xml:id="A76461-001-a-0490">those</w>
      <hi xml:id="A76461-e120">
       <w lemma="office" pos="n2" xml:id="A76461-001-a-0500">Offices</w>
       <pc xml:id="A76461-001-a-0510">,</pc>
      </hi>
      <w lemma="which" pos="crq" xml:id="A76461-001-a-0520">which</w>
      <w lemma="your" pos="po" xml:id="A76461-001-a-0530">your</w>
      <w lemma="quality" pos="n2" rend="hi" xml:id="A76461-001-a-0540">qualities</w>
      <w lemma="oblieg" pos="n1" xml:id="A76461-001-a-0550">oblieg</w>
      <w lemma="you" pos="pn" xml:id="A76461-001-a-0560">you</w>
      <w lemma="to" pos="acp" xml:id="A76461-001-a-0570">to</w>
      <pc unit="sentence" xml:id="A76461-001-a-0580">.</pc>
     </salute>
    </opener>
    <p xml:id="A76461-e140">
     <w lemma="my" pos="po" xml:id="A76461-001-a-0590">My</w>
     <w lemma="hard" pos="j" xml:id="A76461-001-a-0600">hard</w>
     <w lemma="fortune" pos="n1" xml:id="A76461-001-a-0610">fortune</w>
     <w lemma="enforce" pos="vvz" xml:id="A76461-001-a-0620">enforceth</w>
     <w lemma="i" pos="pno" xml:id="A76461-001-a-0630">me</w>
     <w lemma="to" pos="prt" xml:id="A76461-001-a-0640">to</w>
     <w lemma="solicit" pos="vvi" reg="solicit" xml:id="A76461-001-a-0650">solicite</w>
     <w lemma="such" pos="d" xml:id="A76461-001-a-0660">such</w>
     <w lemma="friend" pos="n2" xml:id="A76461-001-a-0670">friends</w>
     <pc xml:id="A76461-001-a-0680">,</pc>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-0690">&amp;</w>
     <w lemma="necessitate" pos="vvz" rend="hi" xml:id="A76461-001-a-0700">necessitates</w>
     <w lemma="i" pos="pno" xml:id="A76461-001-a-0710">me</w>
     <w lemma="thus" pos="av" xml:id="A76461-001-a-0720">thus</w>
     <w lemma="to" pos="prt" xml:id="A76461-001-a-0730">to</w>
     <w lemma="seek" pos="vvi" reg="seek" xml:id="A76461-001-a-0740">seeke</w>
     <w lemma="unto" pos="acp" xml:id="A76461-001-a-0750">unto</w>
     <w lemma="you" pos="pn" xml:id="A76461-001-a-0760">you</w>
     <pc xml:id="A76461-001-a-0770">,</pc>
     <w lemma="that" pos="cs" xml:id="A76461-001-a-0780">that</w>
     <w lemma="you" pos="pn" xml:id="A76461-001-a-0790">you</w>
     <w lemma="will" pos="vmd" xml:id="A76461-001-a-0800">would</w>
     <w lemma="be" pos="vvi" xml:id="A76461-001-a-0810">be</w>
     <w lemma="please" pos="vvn" xml:id="A76461-001-a-0820">pleased</w>
     <w lemma="to" pos="prt" xml:id="A76461-001-a-0830">to</w>
     <w lemma="take" pos="vvi" xml:id="A76461-001-a-0840">take</w>
     <w lemma="notice" pos="n1" xml:id="A76461-001-a-0850">notice</w>
     <w lemma="that" pos="cs" xml:id="A76461-001-a-0860">that</w>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-0870">I</w>
     <w lemma="have" pos="vvb" xml:id="A76461-001-a-0880">have</w>
     <w lemma="suffer" pos="vvn" xml:id="A76461-001-a-0890">suffered</w>
     <w lemma="about" pos="acp" xml:id="A76461-001-a-0900">about</w>
     <w lemma="18" pos="crd" xml:id="A76461-001-a-0910">18</w>
     <w lemma="month" pos="ng2" reg="months'" xml:id="A76461-001-a-0920">monthes</w>
     <hi xml:id="A76461-e160">
      <w lemma="imprisonment" pos="n1" xml:id="A76461-001-a-0930">imprisonment</w>
      <pc xml:id="A76461-001-a-0940">,</pc>
     </hi>
     <w lemma="with" pos="acp" xml:id="A76461-001-a-0950">with</w>
     <w lemma="what" pos="crq" xml:id="A76461-001-a-0960">what</w>
     <w lemma="impair" pos="vvg" xml:id="A76461-001-a-0970">impairing</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-0980">of</w>
     <w lemma="my" pos="po" xml:id="A76461-001-a-0990">my</w>
     <w lemma="subsistence" pos="n1" reg="subsistence" xml:id="A76461-001-a-1000">subsistance</w>
     <pc xml:id="A76461-001-a-1010">,</pc>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-1020">I</w>
     <w lemma="forbear" pos="vvb" reg="forbear" xml:id="A76461-001-a-1030">forbeare</w>
     <pc unit="sentence" xml:id="A76461-001-a-1040">.</pc>
    </p>
    <p xml:id="A76461-e170">
     <hi xml:id="A76461-e180">
      <w lemma="as" pos="acp" xml:id="A76461-001-a-1050">As</w>
      <w lemma="for" pos="acp" xml:id="A76461-001-a-1060">for</w>
      <w lemma="my" pos="po" xml:id="A76461-001-a-1070">my</w>
      <w lemma="condition" pos="n1" xml:id="A76461-001-a-1080">Condition</w>
      <pc xml:id="A76461-001-a-1090">,</pc>
     </hi>
    </p>
    <p xml:id="A76461-e190">
     <w lemma="it" pos="pn" xml:id="A76461-001-a-1100">It</w>
     <w lemma="be" pos="vvz" xml:id="A76461-001-a-1110">is</w>
     <w lemma="such" pos="d" xml:id="A76461-001-a-1120">such</w>
     <w lemma="as" pos="acp" xml:id="A76461-001-a-1130">as</w>
     <w lemma="will" pos="vmb" xml:id="A76461-001-a-1140">will</w>
     <w lemma="soon" pos="av" reg="soon" xml:id="A76461-001-a-1150">soone</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-1160">and</w>
     <w lemma="certain" pos="av-j" xml:id="A76461-001-a-1170">certainly</w>
     <w lemma="destroy" pos="vvi" xml:id="A76461-001-a-1180">destroy</w>
     <w lemma="i" pos="pno" xml:id="A76461-001-a-1190">me</w>
     <pc xml:id="A76461-001-a-1200">,</pc>
     <w lemma="if" pos="cs" xml:id="A76461-001-a-1210">if</w>
     <hi xml:id="A76461-e200">
      <w lemma="divine" pos="j" xml:id="A76461-001-a-1220">divine</w>
      <w lemma="providence" pos="n1" xml:id="A76461-001-a-1230">providence</w>
      <w lemma="prevent" pos="vvi" xml:id="A76461-001-a-1240">prevent</w>
      <w lemma="not" pos="xx" xml:id="A76461-001-a-1250">not</w>
      <pc unit="sentence" xml:id="A76461-001-a-1260">.</pc>
     </hi>
     <w lemma="but" pos="acp" xml:id="A76461-001-a-1270">But</w>
     <w lemma="if" pos="cs" xml:id="A76461-001-a-1280">if</w>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-1290">I</w>
     <w lemma="be" pos="vvb" xml:id="A76461-001-a-1300">be</w>
     <w lemma="destinate" pos="j" xml:id="A76461-001-a-1310">destinate</w>
     <w lemma="to" pos="prt" xml:id="A76461-001-a-1320">to</w>
     <w lemma="ruin" pos="vvi" reg="ruin" xml:id="A76461-001-a-1330">ruine</w>
     <pc xml:id="A76461-001-a-1340">,</pc>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-1350">I</w>
     <w lemma="wish" pos="vvb" xml:id="A76461-001-a-1360">wish</w>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-1370">I</w>
     <w lemma="may" pos="vmd" xml:id="A76461-001-a-1380">might</w>
     <w lemma="perish" pos="vvi" xml:id="A76461-001-a-1390">perish</w>
     <w lemma="by" pos="acp" xml:id="A76461-001-a-1400">by</w>
     <w lemma="some" pos="d" xml:id="A76461-001-a-1410">some</w>
     <w lemma="other" pos="d" xml:id="A76461-001-a-1420">other</w>
     <w lemma="way" pos="n1" xml:id="A76461-001-a-1430">way</w>
     <pc xml:id="A76461-001-a-1440">,</pc>
     <w lemma="rather" pos="avc" xml:id="A76461-001-a-1450">rather</w>
     <w lemma="than" pos="cs" reg="than" xml:id="A76461-001-a-1460">then</w>
     <w lemma="you" pos="pn" xml:id="A76461-001-a-1470">you</w>
     <w lemma="shall" pos="vmd" xml:id="A76461-001-a-1480">should</w>
     <w lemma="be" pos="vvi" xml:id="A76461-001-a-1490">be</w>
     <w lemma="accessary" pos="j" xml:id="A76461-001-a-1500">accessary</w>
     <pc unit="sentence" xml:id="A76461-001-a-1510">.</pc>
    </p>
    <p xml:id="A76461-e210">
     <hi xml:id="A76461-e220">
      <w lemma="as" pos="acp" xml:id="A76461-001-a-1520">As</w>
      <w lemma="for" pos="acp" xml:id="A76461-001-a-1530">for</w>
      <w lemma="my" pos="po" xml:id="A76461-001-a-1540">my</w>
      <w lemma="desire" pos="n2" xml:id="A76461-001-a-1550">desires</w>
      <pc xml:id="A76461-001-a-1560">,</pc>
     </hi>
    </p>
    <p xml:id="A76461-e230">
     <w lemma="my" pos="po" xml:id="A76461-001-a-1570">My</w>
     <w lemma="petition" pos="n1" rend="hi" xml:id="A76461-001-a-1580">petition</w>
     <w lemma="relate" pos="vvz" xml:id="A76461-001-a-1590">relates</w>
     <pc xml:id="A76461-001-a-1600">,</pc>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-1610">and</w>
     <w lemma="my" pos="po" xml:id="A76461-001-a-1620">my</w>
     <w lemma="request" pos="n1" xml:id="A76461-001-a-1630">request</w>
     <pc join="right" xml:id="A76461-001-a-1640">(</pc>
     <w lemma="right" pos="av-j" xml:id="A76461-001-a-1650">Right</w>
     <w lemma="worthy" pos="j" reg="worthy" xml:id="A76461-001-a-1660">worthie</w>
     <pc xml:id="A76461-001-a-1670">)</pc>
     <w lemma="to" pos="acp" xml:id="A76461-001-a-1680">to</w>
     <w lemma="you" pos="pn" xml:id="A76461-001-a-1690">You</w>
     <pc xml:id="A76461-001-a-1700">,</pc>
     <w lemma="be" pos="vvz" xml:id="A76461-001-a-1710">is</w>
     <w lemma="only" pos="av-j" xml:id="A76461-001-a-1720">only</w>
     <w lemma="that" pos="cs" xml:id="A76461-001-a-1730">that</w>
     <w lemma="you" pos="pn" xml:id="A76461-001-a-1740">you</w>
     <w lemma="will" pos="vmd" xml:id="A76461-001-a-1750">would</w>
     <w lemma="present" pos="vvi" xml:id="A76461-001-a-1760">present</w>
     <w lemma="this" pos="d" xml:id="A76461-001-a-1770">this</w>
     <w lemma="postscript-petition" pos="n1" rend="hi" xml:id="A76461-001-a-1780">Postscript-Petition</w>
     <w lemma="to" pos="acp" xml:id="A76461-001-a-1790">to</w>
     <w lemma="the" pos="d" xml:id="A76461-001-a-1800">the</w>
     <w lemma="house" pos="n1" xml:id="A76461-001-a-1810">House</w>
     <pc xml:id="A76461-001-a-1820">,</pc>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-1830">and</w>
     <w lemma="but" pos="acp" xml:id="A76461-001-a-1840">but</w>
     <w lemma="so" pos="av" xml:id="A76461-001-a-1850">so</w>
     <w lemma="far" pos="av-j" xml:id="A76461-001-a-1860">far</w>
     <w lemma="to" pos="prt" xml:id="A76461-001-a-1870">to</w>
     <w lemma="favour" pos="vvi" xml:id="A76461-001-a-1880">favour</w>
     <w lemma="it" pos="pn" xml:id="A76461-001-a-1890">it</w>
     <pc xml:id="A76461-001-a-1900">;</pc>
     <w lemma="as" pos="acp" xml:id="A76461-001-a-1910">as</w>
     <w lemma="you" pos="pn" xml:id="A76461-001-a-1920">you</w>
     <w lemma="feel" pos="vvb" reg="feel" xml:id="A76461-001-a-1930">feele</w>
     <w lemma="yourselves" pos="pr" reg="yourselves" xml:id="A76461-001-a-1940">your selves</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-1960">in</w>
     <w lemma="honour" pos="n1" rend="hi" xml:id="A76461-001-a-1970">honour</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-1980">&amp;</w>
     <w lemma="humanity" pos="n1" reg="humanity" rend="hi" xml:id="A76461-001-a-1990">humanitie</w>
     <w lemma="concern" pos="vvn" xml:id="A76461-001-a-2000">concerned</w>
     <pc unit="sentence" xml:id="A76461-001-a-2010">.</pc>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-2020">And</w>
     <w lemma="if" pos="cs" xml:id="A76461-001-a-2030">if</w>
     <w lemma="any" pos="d" xml:id="A76461-001-a-2040">any</w>
     <w lemma="man" pos="n1" xml:id="A76461-001-a-2050">man</w>
     <w lemma="can" pos="vmb" xml:id="A76461-001-a-2060">can</w>
     <w lemma="object" pos="vvi" xml:id="A76461-001-a-2070">object</w>
     <pc xml:id="A76461-001-a-2080">,</pc>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-2090">and</w>
     <w lemma="convince" pos="vvi" xml:id="A76461-001-a-2100">convince</w>
     <w lemma="i" pos="pno" xml:id="A76461-001-a-2110">me</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-2120">of</w>
     <w lemma="disaffection" pos="n1" xml:id="A76461-001-a-2130">disaffection</w>
     <w lemma="to" pos="acp" xml:id="A76461-001-a-2140">to</w>
     <w lemma="your" pos="po" xml:id="A76461-001-a-2150">your</w>
     <w lemma="general" pos="j" reg="general" xml:id="A76461-001-a-2160">generall</w>
     <w lemma="good" pos="j" xml:id="A76461-001-a-2170">good</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-2180">and</w>
     <w lemma="proceed" pos="n2-vg" xml:id="A76461-001-a-2190">proceedings</w>
     <pc xml:id="A76461-001-a-2200">;</pc>
     <w lemma="or" pos="cc" xml:id="A76461-001-a-2210">or</w>
     <w lemma="that" pos="cs" xml:id="A76461-001-a-2220">that</w>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-2230">I</w>
     <w lemma="have" pos="vvb" xml:id="A76461-001-a-2240">have</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-2250">in</w>
     <w lemma="the" pos="d" xml:id="A76461-001-a-2260">the</w>
     <w lemma="least" pos="ds" xml:id="A76461-001-a-2270">least</w>
     <w lemma="recede" pos="vvd" xml:id="A76461-001-a-2280">receded</w>
     <w lemma="from" pos="acp" xml:id="A76461-001-a-2290">from</w>
     <w lemma="what" pos="crq" xml:id="A76461-001-a-2300">what</w>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-2310">I</w>
     <w lemma="believe" pos="vvb" reg="believe" xml:id="A76461-001-a-2320">beleeve</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A76461-001-a-2330">honorable</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-2340">and</w>
     <w lemma="conducible" pos="j" xml:id="A76461-001-a-2350">conducible</w>
     <pc xml:id="A76461-001-a-2360">,</pc>
     <w lemma="that" pos="cs" xml:id="A76461-001-a-2370">that</w>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-2380">I</w>
     <w lemma="may" pos="vmb" xml:id="A76461-001-a-2390">may</w>
     <w lemma="suffer" pos="vvi" xml:id="A76461-001-a-2400">suffer</w>
     <w lemma="for" pos="acp" xml:id="A76461-001-a-2410">for</w>
     <w lemma="it" pos="pn" xml:id="A76461-001-a-2420">it</w>
     <pc xml:id="A76461-001-a-2430">:</pc>
     <w lemma="if" pos="cs" xml:id="A76461-001-a-2440">If</w>
     <w lemma="not" pos="xx" xml:id="A76461-001-a-2450">not</w>
     <pc xml:id="A76461-001-a-2460">,</pc>
     <w lemma="that" pos="cs" xml:id="A76461-001-a-2470">that</w>
     <w lemma="my" pos="po" xml:id="A76461-001-a-2480">my</w>
     <w lemma="suffering" pos="n2" xml:id="A76461-001-a-2490">sufferings</w>
     <w lemma="may" pos="vmb" xml:id="A76461-001-a-2500">may</w>
     <w lemma="have" pos="vvi" xml:id="A76461-001-a-2510">have</w>
     <w lemma="expiate" pos="vvn" xml:id="A76461-001-a-2520">expiated</w>
     <w lemma="for" pos="acp" xml:id="A76461-001-a-2530">for</w>
     <w lemma="my" pos="po" xml:id="A76461-001-a-2540">my</w>
     <w lemma="diversity" pos="n1" reg="diversity" xml:id="A76461-001-a-2550">diversitie</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-2560">of</w>
     <w lemma="judgement" pos="n1" reg="judgement" xml:id="A76461-001-a-2570">Iudgement</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-2580">in</w>
     <w lemma="case" pos="n1" xml:id="A76461-001-a-2590">case</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-2600">of</w>
     <w lemma="conscience" pos="n1" xml:id="A76461-001-a-2610">Conscience</w>
     <pc unit="sentence" xml:id="A76461-001-a-2620">.</pc>
    </p>
    <p xml:id="A76461-e280">
     <w lemma="thus" pos="av" xml:id="A76461-001-a-2630">Thus</w>
     <w lemma="be" pos="vvg" xml:id="A76461-001-a-2640">being</w>
     <w lemma="reduce" pos="vvn" xml:id="A76461-001-a-2650">reduced</w>
     <w lemma="to" pos="acp" xml:id="A76461-001-a-2660">to</w>
     <w lemma="this" pos="d" xml:id="A76461-001-a-2670">this</w>
     <pc xml:id="A76461-001-a-2680">;</pc>
     <w lemma="either" pos="d" xml:id="A76461-001-a-2690">either</w>
     <w lemma="to" pos="acp" xml:id="A76461-001-a-2700">to</w>
     <w lemma="rot" pos="n1" reg="rot" rend="hi" xml:id="A76461-001-a-2710">Rott</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-2720">in</w>
     <w lemma="a" pos="d" xml:id="A76461-001-a-2730">a</w>
     <w lemma="goal" pos="n1" reg="goal" xml:id="A76461-001-a-2740">Goale</w>
     <pc xml:id="A76461-001-a-2750">,</pc>
     <w lemma="or" pos="cc" xml:id="A76461-001-a-2760">or</w>
     <w lemma="thus" pos="av" xml:id="A76461-001-a-2770">thus</w>
     <w lemma="to" pos="prt" xml:id="A76461-001-a-2780">to</w>
     <w lemma="importune" pos="vvi" xml:id="A76461-001-a-2790">importune</w>
     <w lemma="my" pos="po" xml:id="A76461-001-a-2800">my</w>
     <w lemma="liberty" pos="n1" reg="liberty" xml:id="A76461-001-a-2810">libertie</w>
     <pc xml:id="A76461-001-a-2820">;</pc>
     <w lemma="have" pos="vvg" xml:id="A76461-001-a-2830">having</w>
     <w lemma="no" pos="dx" xml:id="A76461-001-a-2840">no</w>
     <w lemma="other" pos="d" xml:id="A76461-001-a-2850">other</w>
     <w lemma="way" pos="n1" xml:id="A76461-001-a-2860">way</w>
     <w lemma="leave" pos="vvd" xml:id="A76461-001-a-2870">left</w>
     <w lemma="i" pos="pno" xml:id="A76461-001-a-2880">me</w>
     <pc xml:id="A76461-001-a-2890">;</pc>
     <w lemma="this" pos="d" xml:id="A76461-001-a-2900">this</w>
     <w lemma="necessity" pos="n1" xml:id="A76461-001-a-2910">necessity</w>
     <pc join="right" xml:id="A76461-001-a-2920">(</pc>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-2930">I</w>
     <w lemma="hope" pos="vvb" xml:id="A76461-001-a-2940">hope</w>
     <pc xml:id="A76461-001-a-2950">)</pc>
     <w lemma="will" pos="vmb" xml:id="A76461-001-a-2960">will</w>
     <w lemma="justify" pos="vvi" reg="justify" xml:id="A76461-001-a-2970">iustifie</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-2980">and</w>
     <w lemma="excuse" pos="vvi" xml:id="A76461-001-a-2990">excuse</w>
     <w lemma="the" pos="d" xml:id="A76461-001-a-3000">the</w>
     <w lemma="course" pos="n1" xml:id="A76461-001-a-3010">course</w>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-3020">I</w>
     <w lemma="have" pos="vvb" xml:id="A76461-001-a-3030">have</w>
     <w lemma="now" pos="av" xml:id="A76461-001-a-3040">now</w>
     <w lemma="take" pos="vvn" xml:id="A76461-001-a-3050">taken</w>
     <pc unit="sentence" xml:id="A76461-001-a-3060">.</pc>
    </p>
    <p xml:id="A76461-e300">
     <w lemma="so" pos="av" xml:id="A76461-001-a-3070">So</w>
     <w lemma="be" pos="vvg" xml:id="A76461-001-a-3080">being</w>
     <w lemma="you" pos="png" xml:id="A76461-001-a-3090">yours</w>
     <w lemma="both" pos="d" xml:id="A76461-001-a-3100">both</w>
     <w lemma="by" pos="acp" xml:id="A76461-001-a-3110">by</w>
     <w lemma="obligation" pos="n1" xml:id="A76461-001-a-3120">obligation</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-3130">and</w>
     <w lemma="purchase" pos="n1" xml:id="A76461-001-a-3140">purchase</w>
     <pc xml:id="A76461-001-a-3150">,</pc>
     <w lemma="I" pos="pns" xml:id="A76461-001-a-3160">I</w>
     <w lemma="hope" pos="vvb" xml:id="A76461-001-a-3170">hope</w>
     <w lemma="your" pos="po" xml:id="A76461-001-a-3180">your</w>
     <w lemma="goodness" pos="n1" reg="goodness" xml:id="A76461-001-a-3190">goodnesse</w>
     <w lemma="will" pos="vmb" xml:id="A76461-001-a-3200">will</w>
     <w lemma="make" pos="vvi" xml:id="A76461-001-a-3210">make</w>
     <w lemma="i" pos="pno" xml:id="A76461-001-a-3220">me</w>
     <w lemma="i" pos="png" xml:id="A76461-001-a-3230">mine</w>
     <w lemma="own" pos="d" reg="own" xml:id="A76461-001-a-3240">owne</w>
     <pc xml:id="A76461-001-a-3250">;</pc>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-3260">and</w>
     <w lemma="remain" pos="vvi" reg="remain" xml:id="A76461-001-a-3270">remaine</w>
     <pc xml:id="A76461-001-a-3280">,</pc>
    </p>
    <closer xml:id="A76461-e310">
     <signed xml:id="A76461-e320">
      <w lemma="your" pos="po" xml:id="A76461-001-a-3290">Your</w>
      <w lemma="more" pos="avc-d" xml:id="A76461-001-a-3300">more</w>
      <w lemma="and" pos="cc" xml:id="A76461-001-a-3310">and</w>
      <w lemma="more" pos="avc-d" xml:id="A76461-001-a-3320">more</w>
      <w lemma="servant" pos="n1" xml:id="A76461-001-a-3330">servant</w>
      <pc xml:id="A76461-001-a-3340">,</pc>
      <hi xml:id="A76461-e330">
       <w lemma="Paul" pos="nn1" xml:id="A76461-001-a-3350">Paul</w>
       <w lemma="best" pos="js" xml:id="A76461-001-a-3360">Best</w>
       <pc xml:id="A76461-001-a-3370">,</pc>
      </hi>
     </signed>
    </closer>
   </div>
   <div type="petition" xml:id="A76461-e340">
    <head xml:id="A76461-e350">
     <w lemma="to" pos="acp" xml:id="A76461-001-a-3380">To</w>
     <w lemma="the" pos="d" xml:id="A76461-001-a-3390">the</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A76461-001-a-3400">Honorable</w>
     <pc xml:id="A76461-001-a-3410">,</pc>
     <w lemma="the" pos="d" xml:id="A76461-001-a-3420">the</w>
     <w lemma="commons" pos="n2" xml:id="A76461-001-a-3430">Commons</w>
     <w lemma="assemble" pos="vvn" xml:id="A76461-001-a-3440">assembled</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-3450">in</w>
     <w lemma="parliament" pos="n1" xml:id="A76461-001-a-3460">Parliament</w>
     <pc unit="sentence" xml:id="A76461-001-a-3470">.</pc>
     <w lemma="the" pos="d" xml:id="A76461-001-a-3480">The</w>
     <w lemma="petition" pos="n1" xml:id="A76461-001-a-3490">Petition</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-3500">of</w>
     <hi xml:id="A76461-e360">
      <w lemma="Paul" pos="nn1" xml:id="A76461-001-a-3510">Paul</w>
      <w lemma="best" pos="js" xml:id="A76461-001-a-3520">Best</w>
      <pc xml:id="A76461-001-a-3530">,</pc>
     </hi>
     <w lemma="prisoner" pos="n1" xml:id="A76461-001-a-3540">prisoner</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-3550">in</w>
     <w lemma="the" pos="d" xml:id="A76461-001-a-3560">the</w>
     <w lemma="gatehouse" pos="n1" reg="gatehouse" xml:id="A76461-001-a-3570">Gate-house</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-3580">in</w>
     <hi xml:id="A76461-e370">
      <w lemma="Westminster" pos="nn1" xml:id="A76461-001-a-3590">Westminster</w>
      <pc unit="sentence" xml:id="A76461-001-a-3600">.</pc>
     </hi>
    </head>
    <opener xml:id="A76461-e380">
     <w lemma="humble" pos="av-j" xml:id="A76461-001-a-3610">Humbly</w>
     <w lemma="show" pos="vvz" reg="showeth" xml:id="A76461-001-a-3620">sheweth</w>
     <pc unit="sentence" xml:id="A76461-001-a-3630">.</pc>
    </opener>
    <p xml:id="A76461-e390">
     <w lemma="that" pos="cs" xml:id="A76461-001-a-3640">THat</w>
     <w lemma="your" pos="po" xml:id="A76461-001-a-3650">your</w>
     <w lemma="petitioner" pos="n1" xml:id="A76461-001-a-3660">Petitioner</w>
     <w lemma="have" pos="vvz" xml:id="A76461-001-a-3670">hath</w>
     <w lemma="suffer" pos="vvn" xml:id="A76461-001-a-3680">suffered</w>
     <w lemma="a" pos="d" xml:id="A76461-001-a-3690">a</w>
     <w lemma="long" pos="j" xml:id="A76461-001-a-3700">long</w>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-3710">and</w>
     <w lemma="close" pos="j" xml:id="A76461-001-a-3720">close</w>
     <w lemma="imprisonment" pos="n1" xml:id="A76461-001-a-3730">imprisonment</w>
     <pc xml:id="A76461-001-a-3740">,</pc>
     <w lemma="the" pos="d" xml:id="A76461-001-a-3750">the</w>
     <w lemma="cause" pos="n1" xml:id="A76461-001-a-3760">cause</w>
     <w lemma="be" pos="vvg" xml:id="A76461-001-a-3770">being</w>
     <w lemma="sufficient" pos="av-j" xml:id="A76461-001-a-3780">sufficiently</w>
     <w lemma="kowne" pos="j" xml:id="A76461-001-a-3790">kowne</w>
     <w lemma="to" pos="acp" xml:id="A76461-001-a-3800">to</w>
     <w lemma="your" pos="po" xml:id="A76461-001-a-3810">your</w>
     <w lemma="honour" pos="n2" xml:id="A76461-001-a-3820">honours</w>
     <pc unit="sentence" xml:id="A76461-001-a-3830">.</pc>
    </p>
    <p xml:id="A76461-e400">
     <w lemma="wherefore" pos="crq" xml:id="A76461-001-a-3840">Wherefore</w>
     <w lemma="he" pos="pns" xml:id="A76461-001-a-3850">he</w>
     <w lemma="make" pos="vvz" xml:id="A76461-001-a-3860">makes</w>
     <w lemma="it" pos="pn" xml:id="A76461-001-a-3870">it</w>
     <w lemma="his" pos="po" xml:id="A76461-001-a-3880">his</w>
     <w lemma="humble" pos="j" xml:id="A76461-001-a-3890">humble</w>
     <w lemma="suit" pos="n1" xml:id="A76461-001-a-3900">suit</w>
     <w lemma="to" pos="acp" xml:id="A76461-001-a-3910">to</w>
     <w lemma="this" pos="d" xml:id="A76461-001-a-3920">this</w>
     <w lemma="honourable" pos="j" reg="honourable" xml:id="A76461-001-a-3930">Honorable</w>
     <w lemma="house" pos="n1" xml:id="A76461-001-a-3940">House</w>
     <pc xml:id="A76461-001-a-3950">:</pc>
     <w lemma="that" pos="cs" xml:id="A76461-001-a-3960">that</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-3970">in</w>
     <w lemma="consideration" pos="n1" xml:id="A76461-001-a-3980">consideration</w>
     <w lemma="of" pos="acp" xml:id="A76461-001-a-3990">of</w>
     <w lemma="his" pos="po" xml:id="A76461-001-a-4000">his</w>
     <w lemma="service" pos="n1" xml:id="A76461-001-a-4010">service</w>
     <pc xml:id="A76461-001-a-4020">,</pc>
     <w lemma="and" pos="cc" xml:id="A76461-001-a-4030">and</w>
     <w lemma="suffering" pos="n2" xml:id="A76461-001-a-4040">sufferings</w>
     <w lemma="you" pos="pn" xml:id="A76461-001-a-4050">you</w>
     <w lemma="will" pos="vmd" xml:id="A76461-001-a-4060">would</w>
     <w lemma="be" pos="vvi" xml:id="A76461-001-a-4070">be</w>
     <w lemma="please" pos="vvn" xml:id="A76461-001-a-4080">pleased</w>
     <w lemma="to" pos="prt" xml:id="A76461-001-a-4090">to</w>
     <w lemma="release" pos="vvi" xml:id="A76461-001-a-4100">release</w>
     <w lemma="he" pos="pno" xml:id="A76461-001-a-4110">him</w>
     <pc xml:id="A76461-001-a-4120">,</pc>
     <w lemma="or" pos="cc" xml:id="A76461-001-a-4130">or</w>
     <w lemma="grant" pos="vvb" xml:id="A76461-001-a-4140">grant</w>
     <w lemma="he" pos="pno" xml:id="A76461-001-a-4150">him</w>
     <w lemma="a" pos="d" xml:id="A76461-001-a-4160">a</w>
     <w lemma="speedy" pos="j" reg="speedy" xml:id="A76461-001-a-4170">speedie</w>
     <w lemma="hear" pos="n1-vg" xml:id="A76461-001-a-4180">hearing</w>
     <w lemma="as" pos="acp" xml:id="A76461-001-a-4190">as</w>
     <w lemma="your" pos="po" xml:id="A76461-001-a-4200">your</w>
     <w lemma="honour" pos="n2" xml:id="A76461-001-a-4210">Honours</w>
     <w lemma="shall" pos="vmb" xml:id="A76461-001-a-4220">shall</w>
     <w lemma="in" pos="acp" xml:id="A76461-001-a-4230">in</w>
     <w lemma="charity" pos="n1" reg="charity" xml:id="A76461-001-a-4240">charitie</w>
     <w lemma="think" pos="vvb" xml:id="A76461-001-a-4250">think</w>
     <w lemma="fit" pos="vvg" xml:id="A76461-001-a-4260">fitting</w>
     <pc unit="sentence" xml:id="A76461-001-a-4270">.</pc>
    </p>
    <closer xml:id="A76461-e410">
     <dateline xml:id="A76461-e420">
      <date xml:id="A76461-e430">
       <add xml:id="A76461-e440">
        <w lemma="aug" pos="ab" xml:id="A76461-001-a-4280">Aug:</w>
        <w lemma="13" pos="crd" xml:id="A76461-001-a-4300">13</w>
        <w lemma="1646" pos="crd" xml:id="A76461-001-a-4310">1646</w>
       </add>
      </date>
     </dateline>
     <signed xml:id="A76461-e450">
      <w lemma="and" pos="cc" xml:id="A76461-001-a-4320">And</w>
      <w lemma="your" pos="po" xml:id="A76461-001-a-4330">your</w>
      <w lemma="petitioner" pos="n1" xml:id="A76461-001-a-4340">Petitioner</w>
      <w lemma="shall" pos="vmb" xml:id="A76461-001-a-4350">shall</w>
      <w lemma="ever" pos="av" xml:id="A76461-001-a-4360">ever</w>
      <w lemma="pray" pos="vvi" xml:id="A76461-001-a-4370">pray</w>
      <pc xml:id="A76461-001-a-4380">,</pc>
      <w lemma="etc." pos="ab" xml:id="A76461-001-a-4390">&amp;c.</w>
      <pc unit="sentence" xml:id="A76461-001-a-4400"/>
     </signed>
    </closer>
   </div>
  </body>
 </text>
</TEI>

<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="A76043">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>All gentlemen merchants, and others, may please to take notice, that if they send their letters by the old post, ...</title>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription A76043 of text R211682 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Thomason 669.f.16[92]). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 1 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">A76043</idno>
    <idno type="STC">Wing A933</idno>
    <idno type="STC">Thomason 669.f.16[92]</idno>
    <idno type="STC">ESTC R211682</idno>
    <idno type="EEBO-CITATION">99870388</idno>
    <idno type="PROQUEST">99870388</idno>
    <idno type="VID">163245</idno>
    <idno type="PROQUESTGOID">2240931641</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. A76043)</note>
    <note>Transcribed from: (Early English Books Online ; image set 163245)</note>
    <note>Images scanned from microfilm: (Thomason Tracts ; 246:669f16[92])</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>All gentlemen merchants, and others, may please to take notice, that if they send their letters by the old post, ...</title>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>s.n.,</publisher>
      <pubPlace>[London :</pubPlace>
      <date>1653]</date>
     </publicationStmt>
     <notesStmt>
      <note>Title from opening words of text.</note>
      <note>Imprint from Wing.</note>
      <note>Annotation on Thomason copy: "The same 2d of April this was cast about upon the Exchange".</note>
      <note>Reproduction of the original in the British Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Postal service -- England -- Early works to 1800.</term>
     <term>Great Britain -- History -- Commonwealth and Protectorate, 1649-1660 -- Early works to 1800.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>A76043</ep:tcp>
    <ep:estc> R211682</ep:estc>
    <ep:stc> (Thomason 669.f.16[92]). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>All gentlemen merchants, and others, may please to take notice, that if they send their letters by the old post, ...</ep:title>
    <ep:author>anon.</ep:author>
    <ep:publicationYear>1653</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>62</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-06</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-07</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="A76043e-10">
  <body xml:id="A76043e-20">
   <pb facs="tcp:163245:1" rend="simple:additions" xml:id="A76043-001-a"/>
   <div type="text" xml:id="A76043e-30">
    <p xml:id="A76043e-40">
     <w lemma="all" pos="av-d" xml:id="A76043-001-a-0010">ALL</w>
     <w lemma="gentleman" pos="n2" xml:id="A76043-001-a-0020">Gentlemen</w>
     <w lemma="merchant" pos="n2" xml:id="A76043-001-a-0030">Merchants</w>
     <pc xml:id="A76043-001-a-0040">,</pc>
     <w lemma="and" pos="cc" xml:id="A76043-001-a-0050">and</w>
     <w lemma="other" pos="pi2-d" xml:id="A76043-001-a-0060">others</w>
     <pc xml:id="A76043-001-a-0070">,</pc>
     <w lemma="may" pos="vmb" xml:id="A76043-001-a-0080">may</w>
     <w lemma="please" pos="vvi" xml:id="A76043-001-a-0090">please</w>
     <w lemma="to" pos="prt" xml:id="A76043-001-a-0100">to</w>
     <w lemma="take" pos="vvi" xml:id="A76043-001-a-0110">take</w>
     <w lemma="notice" pos="n1" xml:id="A76043-001-a-0120">notice</w>
     <pc xml:id="A76043-001-a-0130">,</pc>
     <w lemma="that" pos="cs" xml:id="A76043-001-a-0140">That</w>
     <w lemma="if" pos="cs" xml:id="A76043-001-a-0150">if</w>
     <w lemma="they" pos="pns" xml:id="A76043-001-a-0160">they</w>
     <w lemma="send" pos="vvb" xml:id="A76043-001-a-0170">send</w>
     <w lemma="their" pos="po" xml:id="A76043-001-a-0180">their</w>
     <w lemma="letter" pos="n2" rend="hi" xml:id="A76043-001-a-0190">Letters</w>
     <w lemma="by" pos="acp" xml:id="A76043-001-a-0200">by</w>
     <w lemma="the" pos="d" xml:id="A76043-001-a-0210">the</w>
     <w lemma="old" pos="j" xml:id="A76043-001-a-0220">Old</w>
     <w lemma="post" pos="n1" xml:id="A76043-001-a-0230">Post</w>
     <pc xml:id="A76043-001-a-0240">,</pc>
     <w lemma="they" pos="pns" xml:id="A76043-001-a-0250">they</w>
     <w lemma="will" pos="vmb" xml:id="A76043-001-a-0260">will</w>
     <w lemma="have" pos="vvi" xml:id="A76043-001-a-0270">have</w>
     <w lemma="a" pos="d" xml:id="A76043-001-a-0280">a</w>
     <w lemma="free" pos="j" xml:id="A76043-001-a-0290">Free</w>
     <w lemma="and" pos="cc" xml:id="A76043-001-a-0300">and</w>
     <w lemma="safe" pos="j" xml:id="A76043-001-a-0310">Safe</w>
     <w lemma="go" pos="vvg" xml:id="A76043-001-a-0320">Going</w>
     <w lemma="and" pos="cc" xml:id="A76043-001-a-0330">and</w>
     <w lemma="come" pos="vvg" reg="coming" xml:id="A76043-001-a-0340">Comming</w>
     <w lemma="as" pos="acp" xml:id="A76043-001-a-0350">as</w>
     <w lemma="former" pos="av-j" xml:id="A76043-001-a-0360">formerly</w>
     <pc xml:id="A76043-001-a-0370">,</pc>
     <w lemma="and" pos="cc" xml:id="A76043-001-a-0380">and</w>
     <w lemma="at" pos="acp" xml:id="A76043-001-a-0390">at</w>
     <w lemma="the" pos="d" xml:id="A76043-001-a-0400">the</w>
     <w lemma="low" pos="js" xml:id="A76043-001-a-0410">Lowest</w>
     <w lemma="rate" pos="n2" xml:id="A76043-001-a-0420">Rates</w>
     <pc xml:id="A76043-001-a-0430">:</pc>
     <w lemma="but" pos="acp" xml:id="A76043-001-a-0440">But</w>
     <w lemma="if" pos="cs" xml:id="A76043-001-a-0450">if</w>
     <w lemma="they" pos="pns" xml:id="A76043-001-a-0460">they</w>
     <w lemma="send" pos="vvb" xml:id="A76043-001-a-0470">send</w>
     <w lemma="by" pos="acp" xml:id="A76043-001-a-0480">by</w>
     <w lemma="those" pos="d" xml:id="A76043-001-a-0490">those</w>
     <w lemma="that" pos="d" xml:id="A76043-001-a-0500">that</w>
     <w lemma="stile" pos="n1" xml:id="A76043-001-a-0510">stile</w>
     <w lemma="themselves" pos="pr" xml:id="A76043-001-a-0520">themselves</w>
     <pc xml:id="A76043-001-a-0530">,</pc>
     <hi xml:id="A76043e-60">
      <w lemma="the" pos="d" xml:id="A76043-001-a-0540">The</w>
      <w lemma="new-vndertaker" pos="n2" xml:id="A76043-001-a-0550">New-Vndertakers</w>
      <pc xml:id="A76043-001-a-0560">,</pc>
     </hi>
     <w lemma="their" pos="po" xml:id="A76043-001-a-0570">Their</w>
     <w lemma="passage" pos="n1" xml:id="A76043-001-a-0580">Passage</w>
     <w lemma="will" pos="vmb" xml:id="A76043-001-a-0590">will</w>
     <w lemma="be" pos="vvi" xml:id="A76043-001-a-0600">be</w>
     <w lemma="interrupt" pos="vvn" xml:id="A76043-001-a-0610">interrupted</w>
     <pc unit="sentence" xml:id="A76043-001-a-0620">.</pc>
    </p>
   </div>
  </body>
 </text>
</TEI>
